package com.school.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order {

    private List<Meal> meals = new ArrayList<>();

//    public Order(List<Meal> meals) {
//        this.meals = meals;
//    }

    //addMealToOrder void
    // removeMealFromOrder void
    // getMeals -> getter

    public void addMealToOrder(Meal meal){
        meals.add(meal);
    }

    public void removeMealFromOrder(Meal meal){
        if (meals.contains(meal)){
            meals.remove(meal);
        }

    }

    public List<Meal> getMeals() {
        return meals;
    }


    public void addMealToOrder(List<Meal> mealList1) {
        meals.addAll(mealList1);
    }
}
