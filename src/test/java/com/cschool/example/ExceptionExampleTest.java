package com.cschool.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExceptionExampleTest {

    private boolean compare(String secret){

        if(secret == null || secret.isEmpty() ){
            throw new IllegalArgumentException("Secret is empty!");
        }
        return "Hello".equalsIgnoreCase(secret);


    }

    @Test
    public void compare_givenEmptySecret_shouldThrowException(){
//        Assertions.assertThrows(IllegalArgumentException.class, () ->{
//            compare("");
//        });
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            compare("");
        });
    }

}