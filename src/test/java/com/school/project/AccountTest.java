package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive() {
        // given
        Account account = new Account();

        //then
        assertFalse(account.isActive(),
                "Check if new account is not active");

        assertThat(account.isActive(), equalTo(false));
        assertThat(account.isActive(), is(false));
    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation() {
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActive(),
                "Check if new account is not active");
    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull() {

        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), nullValue());

    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount() {

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress(new Address("Szkolna", "100a"));

        //then
        assertNotNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), notNullValue());
    }


//    public void setEmailAddress(String emailAddress) {
//        // check it contains @
//        if (emailAddress.contains("@")) {
//            this.emailAddress = emailAddress;
//        } else{
//            throw  new IllegalArgumentException("Email must contain @ character");
//        }
//    }

    @Test
    void givenCorrectEmailAddressShouldBeNotNullValue() {
        //given
        Account account1 = new Account();
        //when
        account1.setEmailAddress("james@gmail.com");

        //then
        assertThat(account1.getEmailAddress(), notNullValue());
    }

    @Test()
    public void checkedEmailWithoutATOrSmallerThan10CharactersShouldThrowException() {
        //given
        Account account2 = new Account();
        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            account2.setEmailAddress("jkrrrwp.pl");
        });
    }

    @ParameterizedTest
    @ValueSource(strings = {"johrrrrrrrn@wp.pl","adamrrrrrr@wp.pl", "prrrrrraul@wp.pl" })
    void testParameters(String str){
        //given
        Account account2 = new Account();
        //when
        account2.setEmailAddress(str);
        //then
        assertThat(account2.getEmailAddress(), notNullValue());
    }


    }

