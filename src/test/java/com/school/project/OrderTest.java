package com.school.project;

import com.sun.org.apache.xpath.internal.operations.Or;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    // just an example
    @Test
    void testAssertArrayEquals(){
        // given
        int [] ints1 = {1,2,3};
        int [] ints2 = {1,2,3};

        //then
        assertArrayEquals(ints1, ints2);
    }

    @Test
    void meatListShouldBeEmptyAfterCreationAnOrder(){
        // given
        Order order = new Order();


        //then
        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo(0));
        assertThat(order.getMeals(), emptyCollectionOf(Meal.class));

    }


    @Test
    void method222(){
        //given
        Order order = new Order();
        Meal meal = new Meal(12, "Burger");

        //when
        order.addMealToOrder(meal);

        //then
        assertThat(order.getMeals().size(), equalTo(1));

    }

    @Test
    void addingMealToOrderShouldIncreaseOrderList(){
        Meal meal1 = new Meal(10, "Pizza");
        Meal meal2 = new Meal(10, "Kebab");

        Order order = new Order();
        order.addMealToOrder(meal1);

        assertThat(order.getMeals(), contains(meal1));
//        assertThat(order.getMeals(), contains(meal2));

        assertThat(order.getMeals(), hasItem(meal1));
        assertThat(order.getMeals().get(0).getPrice(), equalTo(10));
    }

    @Test
    void addAndRemoveMealShouldDecreaseOrderSize(){
        Meal meal1 = new Meal(10, "Pizza");

        Order order = new Order();
        order.addMealToOrder(meal1);
        order.removeMealFromOrder(meal1);

        assertThat(order.getMeals(), not(contains(meal1)));
    }

    @Test
    void methodmealsShouldBeInCorrectOrderAfterAddingThemToOrder (){
        Meal meal1 = new Meal(10, "Pizza");
        Meal meal2 = new Meal(10, "Burger");

        Order order = new Order();
        order.addMealToOrder(meal1);
        order.addMealToOrder(meal2);
//        order.removeMealFromOrder(meal1);


//       !!!!!! obiekty muszą byc w tej samej kolejnosci
        assertThat(order.getMeals(), contains(meal1, meal2));

//      !!!! dla 1 obiektu
        assertThat(order.getMeals(), hasItem(meal1));

//       !!!! dla wielu obiektu
        assertThat(order.getMeals(), hasItems(meal1));

        //!!!!!!! muszą byc wymienione wszystkie obiekty
        assertThat(order.getMeals(), containsInAnyOrder(meal1, meal2));
    }

    @Test
    void testIfTwoOrdersAreTheSame(){
        Meal meal1 = new Meal(10, "Pizza");
        Meal meal2 = new Meal(10, "Burger");
        Meal meal3 = new Meal(10, "Pierogi");

        List<Meal> mealList1 = Arrays.asList(meal1, meal2);
        List<Meal> mealList2 = Arrays.asList(meal1, meal2);
        List<Meal> mealList3 = Arrays.asList(meal2, meal1);
        List<Meal> mealList4 = Arrays.asList(meal2, meal1, meal3);

        Order order1 = new Order();
        Order order2 = new Order();
//        Order order3 = new Order();
//        Order order4 = new Order();

        order1.addMealToOrder(mealList1);
        order2.addMealToOrder(mealList2);

        assertThat(order1.getMeals(), is(order2.getMeals()));
    }
}