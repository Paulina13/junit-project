package com.school.project;

public class User {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length()<5){
            throw new IllegalArgumentException("Name is too short");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<18){
            throw new IllegalArgumentException("User is not on age");
        }
        this.age = age;
    }


}
