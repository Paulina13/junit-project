package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    // METHOD 1
    @Test
    void givenEmptyUserShouldGetNullValue() {
        User user = new User();
        // assertThat(user.getName(), nullValue());
        assertNull(user.getName());
    }

    // METHOD 2
    @Test
    void givenAgeLowerThan18ShouldThrowException() {
        // given
        User user = new User();

        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            user.setAge(15);
        });
    }

    // METHOD 3
    @ParameterizedTest
    @ValueSource(strings = {"Ola", "Adam", "Ela"})
    void testParameters(String str) {
        //given
        User user = new User();

        //then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {

            user.setName(str);
        });
    }

}